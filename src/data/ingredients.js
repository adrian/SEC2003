const ingredients = [
	{
		"label": "Kaolin",
        "id": "kaolin",
		"origine": "Ceradel 2019",
		"composition": [
			{
				"label":"Al2O3",
				"quantity": 1,
				"type": "amphotere"
			},{
				"label":"SiO2",
				"quantity": 1,
				"type": "oxyde_acide"
			}
		],
		"commentaire": "c'est cool",
		"masse_molaire": 258
	},
	{
		"label": "Syenite",
        "id": "syenite",
		"origine": "",
		"composition": [
			{
				"label":"KNaO",
				"quantity": 0.4,
				"type": "oxyde_basique"
			},{
				"label":"Al2O3",
				"quantity": 0.2,
				"type": "amphotere"
			},{
				"label":"SiO2",
				"quantity": 3,
				"type": "oxyde_acide"
			}
		],
		"commentaire": "c'est cool",
		"masse_molaire": 120
	},
    {
		"label": "Talc",
        "id": "talc",
		"origine": "",
		"composition": [
			{
				"label":"BULLSHIT 500",
				"quantity": 1,
				"type": "oxyde_basique"
			},{
				"label":"SiO2",
				"quantity": 3,
				"type": "oxyde_acide"
			}
		],
		"commentaire": "c'est cool",
		"masse_molaire": 120
	},
    {
		"label": "Craie",
        "id": "craie",
		"origine": "",
		"composition": [
			{
				"label":"CAO",
				"quantity": 1,
				"type": "oxyde_basique"
			}
        ],
		"commentaire": "c'est cool",
		"masse_molaire": 120
	}
];

export default ingredients;
